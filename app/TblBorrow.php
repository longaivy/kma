<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TblBorrow extends Model {
    public $incrementing = false;
    public $autoincrement = false;
    protected $table = 'borrow';

}
